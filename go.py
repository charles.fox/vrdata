import pandas as pd
import os

def pp(id_exp=0):

    datadir = "data/"

    fns = os.listdir(datadir)
    fns.sort()

    fn_short=fns[id_exp]
    fn_full = datadir+fn_short

    df = pd.read_csv(fn_full)
    df = df.rename(columns={'car_pos_meter ': 'car_pos_meter', ' ped_pos_meter': 'ped_pos_meter'})

    clf()
    plot( [0,600], [0, 0],  'k' )
    plot ( -1 * df['car_pos_meter'] , 'm' )
    plot ( 10*df['ped_pos_meter'] , 'g' )
    xlabel('time')
    ylabel('pos/meters')
    title(fn_short)
